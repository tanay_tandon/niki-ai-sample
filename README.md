Requirements

Android Studio 3.5.1 or above

What this project does

1) Display a list of all the figures that were drawn by the user (com.sample.tanay.drawrects.ui.board.list.BoardListFragment)

2) Allows user to view a figure that they had drawn (com.sample.tanay.drawrects.ui.board.detail.BoardDetailFragment)

3) Allows user to add a new figure and draw as many squares in that figure (com.sample.tanay.drawrects.ui.board.add.AddNewBoardFragment)

4) View the coordinates of all the squares in the figure (com.sample.tanay.drawrects.ui.board.coordinates.DrawingCoordinatesListDialog)


Logic about the dimension of the square

1) The length depends on the dimensions of the device. 

2) The maximum value of the length is half of the smaller dimension of the device, 
ie. if device dimension are 1920 x 1320 then the maximum length will be 660.

3) The length which is displayed on the screen (AddNewBoardFragment) is in dp not pixel


Known issues

1) If length_of_square + position_of_touch_event > screen size a cut square will be drawn.

2) Test coverage is very low