package com.sample.tanay.drawrects.di.board.coordinates

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import com.sample.tanay.drawrects.repository.AppRepository
import com.sample.tanay.drawrects.ui.board.BoardViewModel
import com.sample.tanay.drawrects.ui.board.BoardViewModelFactory
import com.sample.tanay.drawrects.ui.board.coordinates.CoordinateListAdapter
import dagger.Module
import dagger.Provides

@Module
class RectangleCoordinateModule(var activity: FragmentActivity) {

    @Provides
    fun providesRectangleCoordinateAdapter(): CoordinateListAdapter {
        return CoordinateListAdapter(activity)
    }

}