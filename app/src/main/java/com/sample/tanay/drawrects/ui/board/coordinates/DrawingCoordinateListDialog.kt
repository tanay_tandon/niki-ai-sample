package com.sample.tanay.drawrects.ui.board.coordinates

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sample.tanay.drawrects.R
import com.sample.tanay.drawrects.di.board.coordinates.RectangleCoordinateModule
import com.sample.tanay.drawrects.model.Board
import com.sample.tanay.drawrects.ui.base.BaseDialog
import kotlinx.android.synthetic.main.dialog_board_coordinate_list.*
import javax.inject.Inject

class DrawingCoordinateListDialog : BaseDialog() {

    private var board: Board? = null

    @Inject
    lateinit var mAdapter: CoordinateListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            board = it.getParcelable<Board>("board")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_board_coordinate_list, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        getAppComponent().inject(RectangleCoordinateModule(activity!!)).inject(this)
        tvTitle.text = board?.drawing?.getDescription(resources.getString(R.string.item_prefix))
        setupRecyclerView()
        mAdapter.setData(board?.rectangles)
    }

    private fun setupRecyclerView() {
        rvRectCoordinates.adapter = mAdapter
        rvRectCoordinates.layoutManager =
            LinearLayoutManager(activity!!, RecyclerView.VERTICAL, false)
        rvRectCoordinates.setHasFixedSize(true)
    }

}