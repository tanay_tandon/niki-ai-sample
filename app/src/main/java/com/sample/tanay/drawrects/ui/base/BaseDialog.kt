package com.sample.tanay.drawrects.ui.base

import androidx.fragment.app.DialogFragment
import com.sample.tanay.drawrects.di.AppComponent

abstract class BaseDialog : DialogFragment() {

    fun getAppComponent(): AppComponent {
        return (activity as BaseActivity).getAppComponent()
    }
}