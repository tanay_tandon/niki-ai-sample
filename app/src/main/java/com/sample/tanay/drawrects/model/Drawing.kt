package com.sample.tanay.drawrects.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.sample.tanay.drawrects.util.TimeUtils
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity
data class Drawing(
    @PrimaryKey(autoGenerate = true)
    var id: Long? = null,
    var createdAt: Long? = null
) : Parcelable {

    fun getDescription(prefix: String): String {
        return "$prefix $id"
    }

    fun getDateTime(): String {
        return TimeUtils.getDatetimeFromTimestamp(createdAt)
    }
}