package com.sample.tanay.drawrects.ui.board

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.sample.tanay.drawrects.repository.AppRepository

class BoardViewModelFactory(var appRepository: AppRepository) :
    ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (BoardViewModel::class.java.isAssignableFrom(modelClass)) {
            return BoardViewModel(appRepository) as T
        }
        return super.create(modelClass)
    }
}