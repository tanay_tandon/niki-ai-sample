package com.sample.tanay.drawrects.di.board.list

import com.sample.tanay.drawrects.ui.board.list.BoardListFragment
import dagger.Subcomponent

@Subcomponent(modules = arrayOf(BoardListModule::class))
interface BoardListComponent {

    fun inject(fragment: BoardListFragment)
}