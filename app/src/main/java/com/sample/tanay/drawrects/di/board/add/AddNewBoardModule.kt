package com.sample.tanay.drawrects.di.board.add

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import com.sample.tanay.drawrects.ui.board.BoardViewModel
import com.sample.tanay.drawrects.ui.board.BoardViewModelFactory
import com.sample.tanay.drawrects.repository.AppRepository
import dagger.Module
import dagger.Provides

@Module
class AddNewBoardModule(var activity: FragmentActivity) {


    @Provides
    fun provideFactory(appRepository: AppRepository): BoardViewModelFactory {
        return BoardViewModelFactory(appRepository)
    }

    @Provides
    fun provideBoardListViewModel(factory: BoardViewModelFactory): BoardViewModel {
        return ViewModelProvider(activity, factory)[BoardViewModel::class.java]
    }
}