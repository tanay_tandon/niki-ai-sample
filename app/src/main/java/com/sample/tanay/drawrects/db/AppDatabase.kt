package com.sample.tanay.drawrects.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.sample.tanay.drawrects.model.Drawing
import com.sample.tanay.drawrects.model.Rectangle

@Database(
    entities = arrayOf(Drawing::class, Rectangle::class),
    version = 1,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun getItemDao(): DrawingDao

    companion object {

        @Volatile
        private var instance: AppDatabase? = null

        fun instance(
            context: Context,
            databaseName: String
        ): AppDatabase {
            val tempInstance = instance
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val database = Room.databaseBuilder(
                    context, AppDatabase::class.java,
                    databaseName
                ).build()
                instance = database
            }
            return instance!!
        }
    }
}