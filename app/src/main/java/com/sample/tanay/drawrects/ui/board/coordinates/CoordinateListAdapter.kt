package com.sample.tanay.drawrects.ui.board.coordinates

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.sample.tanay.drawrects.R
import com.sample.tanay.drawrects.model.Rectangle
import kotlinx.android.synthetic.main.item_rect_coordinate.view.*

class CoordinateListAdapter(var activity: FragmentActivity) :
    RecyclerView.Adapter<CoordinateListAdapter.CoordinateViewHolder>() {

    private val mRectangles: MutableList<Rectangle> = mutableListOf()

    fun setData(rectangles: MutableList<Rectangle>?) {
        rectangles?.let {
            mRectangles.clear()
            mRectangles.addAll(rectangles)
            notifyDataSetChanged()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CoordinateViewHolder {
        return CoordinateViewHolder(
            LayoutInflater.from(activity).inflate(R.layout.item_rect_coordinate, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return mRectangles.size
    }

    override fun onBindViewHolder(holder: CoordinateViewHolder, position: Int) {
        holder.bind(mRectangles[position])
    }

    class CoordinateViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bind(rectangle: Rectangle) {
            itemView.let {
                it.tvCoordinates.text = rectangle.getCoordinate()
            }
        }
    }
}