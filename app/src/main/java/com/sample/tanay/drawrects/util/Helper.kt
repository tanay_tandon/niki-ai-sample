package com.sample.tanay.drawrects.util

import android.graphics.Point
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Size
import android.util.TypedValue
import androidx.fragment.app.Fragment
import kotlin.math.roundToInt

class Helper : Fragment() {

    companion object {
        const val TAG = "Helper"
    }

    private var mSize: Point? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    fun convertDpToPx(dp: Int): Float {
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            dp.toFloat(),
            resources.displayMetrics
        )
    }

    fun convertPxToDp(px: Double): Int {
        val metrics = DisplayMetrics()
        activity?.windowManager?.defaultDisplay?.getMetrics(metrics)
        return Math.ceil(px / metrics.density).roundToInt()
    }

    fun getScreenHeight(): Int {
        setupSize()
        return mSize?.y!!
    }

    fun getScreenWidth(): Int {
        setupSize()
        return mSize?.x!!
    }

    fun getSmallerDimension(): Int {
        setupSize()
        return convertPxToDp((Math.min(getScreenHeight(), getScreenWidth()) / 2).toDouble())

    }

    private fun setupSize() {
        if (mSize == null) {
            mSize = Point()
            activity?.windowManager?.defaultDisplay?.getSize(mSize)
        }
    }

}