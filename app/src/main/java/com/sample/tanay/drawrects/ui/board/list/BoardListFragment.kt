package com.sample.tanay.drawrects.ui.board.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sample.tanay.drawrects.R
import com.sample.tanay.drawrects.di.board.list.BoardListModule
import com.sample.tanay.drawrects.model.Board
import com.sample.tanay.drawrects.ui.base.BaseFragment
import com.sample.tanay.drawrects.ui.board.BoardViewModel
import kotlinx.android.synthetic.main.fragment_item_list.*
import javax.inject.Inject

class BoardListFragment : BaseFragment(),
    BoardListAdapter.BoardItemListener {
    override fun onItemSelected(position: Int, board: Board) {
        addFragmentToViewDrawing(board)
    }

    override fun onViewCoordinatesSelected(position: Int, board: Board) {
        viewAllCoordinates(board)
    }

    @Inject
    lateinit var viewModel: BoardViewModel

    @Inject
    lateinit var mBoardAdapter: BoardListAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_item_list, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        getAppComponent().inject(
            BoardListModule(
                activity!!,
                this
            )
        ).inject(this)

        setupList()

        fabAddBoard.setOnClickListener { addFragmentToCreateNewDrawing() }

        btnAddDrawing.setOnClickListener {
            addFragmentToCreateNewDrawing()
        }

        viewModel.storedBoards.observe(this, Observer {
            emptyGroup.visibility = if (it.isEmpty()) View.VISIBLE else View.GONE
            mBoardAdapter.setData(it)
        })
    }

    fun setupList() {
        val layoutManager = LinearLayoutManager(context!!, RecyclerView.VERTICAL, false)
        rvBoards.layoutManager = layoutManager
        rvBoards.setHasFixedSize(true)
        rvBoards.adapter = mBoardAdapter
    }

    private fun addFragmentToCreateNewDrawing() {
        findNavController().navigate(BoardListFragmentDirections.actionBoardListFragmentToAddNewBoardFragment())
    }

    private fun viewAllCoordinates(board: Board?) {
        findNavController().navigate(
            BoardListFragmentDirections.actionBoardListFragmentToDrawingCoordinateListDialog(
                board
            )
        )
    }

    private fun addFragmentToViewDrawing(board: Board?) {
        findNavController().navigate(
            BoardListFragmentDirections.actionBoardListFragmentToViewBoardFragment(
                board
            )
        )
    }
}