package com.sample.tanay.drawrects.ui.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.sample.tanay.drawrects.DemoApp
import com.sample.tanay.drawrects.R
import com.sample.tanay.drawrects.di.AppComponent
import com.sample.tanay.drawrects.util.Helper

abstract class BaseActivity : AppCompatActivity() {

     val helper = Helper()
        get() {
            return field
        }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null)
            supportFragmentManager.beginTransaction().add(helper, Helper.TAG).commit()
    }


    fun getAppComponent(): AppComponent {
        return (applicationContext as DemoApp).getAppComponent()
    }

    fun isTwoPane(): Boolean {
        return resources.getBoolean(R.bool.isTwoPane)
    }
}