package com.sample.tanay.drawrects.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize
import kotlin.random.Random

@Entity
@Parcelize
data class Rectangle(
    @PrimaryKey(autoGenerate = true)
    var id: Long? = null,
    var x: Int,
    var y: Int,
    var length: Int,
    var color: String,
    var drawingId: Long? = null
) : Parcelable {

    fun getCoordinate(): String {
        val endX = x + length
        val endY = y + length
        return "($x,$y),($endX, $y), ($x, $endY),($endX, $endY)"
    }

    companion object {

        fun dummyInstance(): Rectangle {
            return Rectangle(
                x = Random.nextInt(100),
                y = Random.nextInt(100),
                length = Random.nextInt(1000),
                color = "#abc"
            )
        }
    }

}