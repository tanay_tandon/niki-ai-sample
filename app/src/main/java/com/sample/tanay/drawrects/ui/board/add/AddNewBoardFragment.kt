package com.sample.tanay.drawrects.ui.board.add

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import android.widget.Toast
import com.sample.tanay.drawrects.R
import com.sample.tanay.drawrects.di.board.add.AddNewBoardModule
import com.sample.tanay.drawrects.model.Board
import com.sample.tanay.drawrects.model.Drawing
import com.sample.tanay.drawrects.ui.base.BaseFragment
import com.sample.tanay.drawrects.ui.board.BoardViewModel
import kotlinx.android.synthetic.main.fragment_add_item.*
import javax.inject.Inject

class AddNewBoardFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: BoardViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_add_item, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        getAppComponent().inject(AddNewBoardModule(activity!!)).inject(this)
        btnSave.setOnClickListener {
            if (drawingBoard.mRectangles.isEmpty()) {
                Toast.makeText(activity!!, R.string.drawOneFigure, Toast.LENGTH_SHORT).show()
            } else {
                saveBoard()
            }
        }
        sbLength.max = 19
        val dimension = getHelper().getSmallerDimension() / 20
        sbLength.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                val length = dimension * (progress + 1)
                drawingBoard.mLength = getHelper().convertDpToPx(length).toInt()
                tvLength.text = length.toString()
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }
        })
        tvLength.text = dimension.toString()
        drawingBoard.mLength = getHelper().convertDpToPx(dimension).toInt()
    }

    private fun saveBoard() {
        val board = Board(
            drawing = Drawing(createdAt = System.currentTimeMillis()),
            rectangles = drawingBoard.mRectangles
        )
        viewModel.insert(board)
        activity?.onBackPressed()
    }
}