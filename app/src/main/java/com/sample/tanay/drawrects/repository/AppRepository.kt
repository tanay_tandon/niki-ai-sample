package com.sample.tanay.drawrects.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.sample.tanay.drawrects.db.DrawingDao
import com.sample.tanay.drawrects.model.Board
import javax.inject.Inject

class AppRepository @Inject constructor(private var drawingDao: DrawingDao) {

    fun getBoards(): LiveData<List<Board>> {
        return drawingDao.getAllItems()
    }

    suspend fun saveBoard(board: Board) {
        board.drawing.id = drawingDao.insert(board.drawing)
        board.updateDrawingIdInRectangles()
        drawingDao.insert(board.rectangles)
    }
}