package com.sample.tanay.drawrects.ui.board

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sample.tanay.drawrects.model.Board
import com.sample.tanay.drawrects.repository.AppRepository
import kotlinx.coroutines.launch

class BoardViewModel constructor(var repository: AppRepository) : ViewModel() {

    val storedBoards = repository.getBoards()


    fun insert(board: Board) {
        viewModelScope.launch {
            repository.saveBoard(board)
        }
    }
}