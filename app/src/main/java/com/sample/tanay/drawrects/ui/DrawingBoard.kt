package com.sample.tanay.drawrects.ui

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import com.sample.tanay.drawrects.model.Rectangle

class DrawingBoard(context: Context, attrs: AttributeSet) :
    View(context, attrs), View.OnTouchListener {

    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        if (mEditMode) {
            val rectangle = Rectangle(
                x = event?.x?.toInt()!!,
                y = event.y.toInt(),
                length = mLength,
                color = mHexColorString
            )
            mRectangles.add(rectangle)
            v?.invalidate()
        }
        return true
    }

    init {
        setOnTouchListener(this)
    }

    private val rect = Rect()
    private val paint = Paint()
    private var mHexColorString = "#000000"
        set(value) {
            field = value
        }

    var mRectangles: MutableList<Rectangle> = mutableListOf()
        set(value) {
            field.clear()
            field.addAll(value)
            invalidate()
        }
        get() {
            return field
        }

    var mEditMode: Boolean = true
        set(value) {
            field = value
        }
        get() {
            return field
        }

    var mLength: Int = 1000
        set(value) {
            field = value
        }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        for (rectangle in mRectangles) {
            rect.set(
                rectangle.x,
                rectangle.y,
                rectangle.x + rectangle.length,
                rectangle.y + rectangle.length
            )
            paint.color = Color.parseColor(rectangle.color)
            canvas?.drawRect(rect, paint)
        }
    }

}