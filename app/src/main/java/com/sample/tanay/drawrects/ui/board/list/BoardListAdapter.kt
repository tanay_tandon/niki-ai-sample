package com.sample.tanay.drawrects.ui.board.list

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.sample.tanay.drawrects.R
import com.sample.tanay.drawrects.model.Board
import kotlinx.android.synthetic.main.item_list_content.view.*

class BoardListAdapter(
    var mBoardItemListener: BoardItemListener,
    var context: Context
) :
    RecyclerView.Adapter<BoardListAdapter.BoardListItemViewHolder>() {

    val mBoards: MutableList<Board> = mutableListOf()

    fun setData(boards: List<Board>) {
        mBoards.clear()
        mBoards.addAll(boards)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BoardListItemViewHolder {
        return BoardListItemViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_list_content,
                parent,
                false
            ),
            mBoardItemListener
        )
    }

    override fun getItemCount(): Int {
        return mBoards.size
    }

    override fun onBindViewHolder(holder: BoardListItemViewHolder, position: Int) {
        holder.bind(position, mBoards[position])
    }

    class BoardListItemViewHolder(view: View, var mBoardItemListener: BoardItemListener) :
        RecyclerView.ViewHolder(view) {

        fun bind(position: Int, board: Board) {
            itemView.setOnClickListener {
                mBoardItemListener.onItemSelected(position, board)
            }
            itemView.btnViewAllCoordinates.setOnClickListener {
                mBoardItemListener.onViewCoordinatesSelected(position, board)
            }
            itemView.tvDrawingTime.text = board.drawing.getDateTime()
            itemView.id_text.text =
                board.drawing.getDescription(itemView.context.resources.getString(R.string.item_prefix))
            itemView.content.text =
                board.getCoordinates(itemView.context.resources.getString(R.string.coordinates))
        }
    }

    interface BoardItemListener {
        fun onItemSelected(position: Int, board: Board)

        fun onViewCoordinatesSelected(position: Int, board: Board)
    }
}