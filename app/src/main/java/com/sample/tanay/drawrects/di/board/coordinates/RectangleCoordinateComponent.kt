package com.sample.tanay.drawrects.di.board.coordinates

import com.sample.tanay.drawrects.ui.board.coordinates.DrawingCoordinateListDialog
import dagger.Subcomponent

@Subcomponent(modules = arrayOf(RectangleCoordinateModule::class))
interface RectangleCoordinateComponent {

    fun inject(dialog: DrawingCoordinateListDialog)
}