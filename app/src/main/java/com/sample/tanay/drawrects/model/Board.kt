package com.sample.tanay.drawrects.model

import android.os.Parcelable
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Relation
import kotlinx.android.parcel.Parcelize
import java.lang.StringBuilder

@Parcelize
data class Board(
    @Embedded
    val drawing: Drawing,
    @Relation(
        parentColumn = "id",
        entityColumn = "drawingId"
    )
    val rectangles: MutableList<Rectangle>
) : Parcelable {

    fun getCoordinates(prefix: String): String {
        val limit =
            if (rectangles.size >= MAX_COORDINATE_COUNT) MAX_COORDINATE_COUNT else rectangles.size
        val stringBuilder = StringBuilder(prefix)
        for (index in 0 until limit) {
            stringBuilder.append('[')
            stringBuilder.append(rectangles[index].getCoordinate())
            stringBuilder.append(']')
        }
        return stringBuilder.toString()
    }

    fun updateDrawingIdInRectangles() {
        for (rectangle in rectangles) {
            rectangle.drawingId = drawing.id
        }
    }

    companion object {
        val MAX_COORDINATE_COUNT = 2
    }

}