package com.sample.tanay.drawrects.di.board.add

import com.sample.tanay.drawrects.ui.board.add.AddNewBoardFragment
import dagger.Subcomponent

@Subcomponent(modules = arrayOf(AddNewBoardModule::class))
interface AddNewBoardComponent {

    fun inject(addNewBoardFragment: AddNewBoardFragment)
}