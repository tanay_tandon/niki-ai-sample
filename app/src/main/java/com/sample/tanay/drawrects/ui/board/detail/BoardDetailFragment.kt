package com.sample.tanay.drawrects.ui.board.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.sample.tanay.drawrects.R
import com.sample.tanay.drawrects.model.Board
import com.sample.tanay.drawrects.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_item_detail.*

/**
 * A fragment representing a single Drawing detail screen.
 * This fragment is either contained in a [BoardListActivity]
 * in two-pane mode (on tablets) or a [BoardDetailActivity]
 * on handsets.
 */
class BoardDetailFragment : BaseFragment() {

    private var board: Board? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            board = it.getParcelable("board")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_item_detail, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        drawingBoard.mEditMode = false
        tvTitle.text = board?.drawing?.getDescription(resources.getString(R.string.item_prefix))
        tvDrawingTime.text = board?.drawing?.getDateTime()
        board?.rectangles?.let {
            drawingBoard.mRectangles = it
        }
        btnViewAllCoordinates.setOnClickListener {
            findNavController().navigate(
                BoardDetailFragmentDirections.actionViewBoardFragmentToDrawingCoordinateListDialog(
                    board
                )
            )
        }
    }
}
