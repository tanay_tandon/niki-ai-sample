package com.sample.tanay.drawrects.di

import com.sample.tanay.drawrects.di.board.add.AddNewBoardComponent
import com.sample.tanay.drawrects.di.board.add.AddNewBoardModule
import com.sample.tanay.drawrects.di.board.coordinates.RectangleCoordinateComponent
import com.sample.tanay.drawrects.di.board.coordinates.RectangleCoordinateModule
import com.sample.tanay.drawrects.di.board.list.BoardListComponent
import com.sample.tanay.drawrects.di.board.list.BoardListModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AppModule::class))
interface AppComponent {

    fun inject(boardListModule: BoardListModule): BoardListComponent

    fun inject(addNewBoardModule: AddNewBoardModule): AddNewBoardComponent

    fun inject(coordinateModule: RectangleCoordinateModule): RectangleCoordinateComponent
}