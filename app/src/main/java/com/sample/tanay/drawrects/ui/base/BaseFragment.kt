package com.sample.tanay.drawrects.ui.base

import androidx.fragment.app.Fragment
import com.sample.tanay.drawrects.di.AppComponent
import com.sample.tanay.drawrects.util.Helper

abstract class BaseFragment() : Fragment() {

    fun getAppComponent(): AppComponent {
        return (activity as BaseActivity).getAppComponent()
    }

    fun isTwoPane(): Boolean {
        return (activity as BaseActivity).isTwoPane()
    }

    fun getHelper(): Helper {
        return (activity as BaseActivity).helper
    }

}