package com.sample.tanay.drawrects.di.board.list

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import com.sample.tanay.drawrects.ui.board.list.BoardListAdapter
import com.sample.tanay.drawrects.ui.board.BoardViewModel
import com.sample.tanay.drawrects.ui.board.BoardViewModelFactory
import com.sample.tanay.drawrects.repository.AppRepository
import dagger.Module
import dagger.Provides

@Module
class BoardListModule(
    var actvity: FragmentActivity,
    var mBoardItemListener: BoardListAdapter.BoardItemListener? = null
) {


    @Provides
    fun provideFactory(appRepository: AppRepository): BoardViewModelFactory {
        return BoardViewModelFactory(appRepository)
    }

    @Provides
    fun provideBoardListViewModel(factory: BoardViewModelFactory): BoardViewModel {
        return ViewModelProvider(actvity, factory)[BoardViewModel::class.java]
    }

    @Provides
    fun provideBoardListAdapter(): BoardListAdapter {
        return BoardListAdapter(
            mBoardItemListener!!,
            actvity
        )
    }

}