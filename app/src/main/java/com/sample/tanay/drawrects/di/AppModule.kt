package com.sample.tanay.drawrects.di

import android.app.Application
import android.content.Context
import com.sample.tanay.drawrects.db.AppDatabase
import com.sample.tanay.drawrects.db.DrawingDao
import dagger.Module
import dagger.Provides

@Module
class AppModule(var application: Application) {

    @Provides
    fun provideAppContext(): Context {
        return application
    }

    @Provides
    fun provideDatabase(context: Context): AppDatabase {
        return AppDatabase.instance(context, "app_db")
    }

    @Provides
    fun provideItemDao(appDatabase: AppDatabase): DrawingDao {
        return appDatabase.getItemDao()
    }
}