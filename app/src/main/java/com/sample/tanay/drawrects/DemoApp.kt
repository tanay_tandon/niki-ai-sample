package com.sample.tanay.drawrects

import android.app.Application
import com.sample.tanay.drawrects.di.AppComponent
import com.sample.tanay.drawrects.di.AppModule
import com.sample.tanay.drawrects.di.DaggerAppComponent

class DemoApp : Application() {

    private lateinit var appComponent: AppComponent


    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder().appModule(AppModule(this)).build()
    }

    fun getAppComponent(): AppComponent {
        return appComponent
    }

}