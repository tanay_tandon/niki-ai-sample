package com.sample.tanay.drawrects.util

import java.text.SimpleDateFormat
import java.util.*
import kotlin.random.Random

object TimeUtils {

    val DATETIME_FORMAT = "EEEE, dd MMM yyyy, HH:mm z"

    val sdf = SimpleDateFormat(DATETIME_FORMAT, Locale.getDefault())

    fun getDatetimeFromTimestamp(timestamp: Long?): String {
        return sdf.format(Date(timestamp!!))
    }

    fun Int.randomInt(): Int {
        return Random.nextInt(0, 100)
    }
}