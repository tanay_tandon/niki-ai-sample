package com.sample.tanay.drawrects.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.sample.tanay.drawrects.model.Board
import com.sample.tanay.drawrects.model.Drawing
import com.sample.tanay.drawrects.model.Rectangle

@Dao
interface DrawingDao {

    @Query("select * from Drawing order by createdAt desc")
    fun getAllItems(): LiveData<List<Board>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(drawing: Drawing): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(rectangles: MutableList<Rectangle>): Array<Long>
}