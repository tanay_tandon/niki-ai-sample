package com.sample.tanay.drawrects

import com.sample.tanay.drawrects.model.Board
import com.sample.tanay.drawrects.model.Drawing
import com.sample.tanay.drawrects.model.Rectangle
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import java.lang.StringBuilder

class BoardUnitTest {

    private lateinit var mBoard: Board
    private var mExpectedResult: String? = null

    @Before
    fun setup() {
        val prefix = "prefix "
        val stringBuilder = StringBuilder(prefix)
        val rectangles = mutableListOf<Rectangle>()
        for (index in 0..4) {
            val rectangle = Rectangle.dummyInstance()
            rectangles.add(rectangle)
        }
        for (index in 0..Board.MAX_COORDINATE_COUNT) {
            stringBuilder.append('[')
            stringBuilder.append(rectangles[index].getCoordinate())
            stringBuilder.append(']')
        }
        mBoard = Board(drawing = Drawing(12, System.currentTimeMillis()), rectangles = rectangles)
        mExpectedResult = stringBuilder.toString()
    }

    @Test
    fun testGetCoordinates() {
        assertEquals(mExpectedResult, mBoard.getCoordinates("prefix "))
    }

    @Test
    fun testUpdateDrawingIdInRectangles() {
        mBoard.drawing.id = 133
        mBoard.updateDrawingIdInRectangles()
        for (rectangle in mBoard.rectangles) {
            assertEquals(rectangle.drawingId, mBoard.drawing.id)
        }
    }

}