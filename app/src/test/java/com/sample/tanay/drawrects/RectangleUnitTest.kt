package com.sample.tanay.drawrects

import com.sample.tanay.drawrects.model.Rectangle
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test


class RectangleUnitTest {

    private lateinit var rectangle: Rectangle
    private val x: Int = 100
    private val y: Int = 100
    private val length: Int = 500
    private val endX: Int = x + length
    private val endY: Int = y + length
    private lateinit var text: String

    @Before
    fun setup() {
        rectangle = Rectangle(x = x, y = y, length = length, color = "#000")
        text = "($x,$y),($endX, $y), ($x, $endY),($endX, $endY)"
    }

    @Test
    fun testGetCoordinate() {
        assertEquals(text, rectangle.getCoordinate())
    }

}