package com.sample.tanay.drawrects

import com.sample.tanay.drawrects.model.Drawing
import com.sample.tanay.drawrects.util.TimeUtils
import org.junit.Test
import java.text.SimpleDateFormat
import java.util.*
import org.junit.Assert.*
import org.junit.Before

class DrawingUnitTest {


    private var expectedDatetimeValue: String? = null
    private var expectedDescription: String? = null
    private lateinit var drawing: Drawing

    @Before
    fun setup() {
        val sdf = SimpleDateFormat(TimeUtils.DATETIME_FORMAT, Locale.getDefault())
        val id: Long = 12
        val currentTime = System.currentTimeMillis()
        expectedDatetimeValue = sdf.format(Date(currentTime))
        expectedDescription = "prefix $id"
        drawing = Drawing(id, createdAt = currentTime)
    }

    @Test
    fun testDateFormat() {
        assertEquals(drawing.getDateTime(), expectedDatetimeValue)
    }

    @Test
    fun testDrawingDescription() {
        assertEquals(drawing.getDescription("prefix"), expectedDescription)
    }

}